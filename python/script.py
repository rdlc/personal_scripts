import requests
import json

if __name__ == '__main__':
    url = 'https://restcountries.eu/rest/v2/all'
    
    reponse = requests.get(url)
    print(reponse)

    if reponse.status_code == 200:
        response_json = json.loads(reponse.text)

        _file = open('file.py', 'w')
        for country in response_json:
            name = str(country['name'])
            img_url = str(country['flag'])
            line = 'country = Country(name="{}", image_url="{}")\n'.format(name, img_url)
            _file.write(line)
            _file.write('self.db.session.add(country)\n')
            _file.write('self.db.session.commit()\n\n')
        _file.close()
    print('The file was write succesful.')
